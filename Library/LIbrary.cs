﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryExercise
{
    public class LIbrary
    {
        Print print = new Print();
        private UsersServerSide UsersServerSide;
        private BooksServerSide booksServerSide;
        public LIbrary()
        {
            this.UsersServerSide = new UsersServerSide();
            this.booksServerSide = new BooksServerSide();
        }

        public void RegisterNewClient(string firstname, string lastname, string id)
        {
            UsersServerSide.AddClient(firstname, lastname, id);
        }

        public void RemoveClient(string id)
        {
            UsersServerSide.RemoveClient(id);
        }

        public void AddBookToLibrary(string bookname)
        {
            booksServerSide.AddBookToLibrary(bookname);
        }

        public void ClientBorrowedBook(string userid, string bookid)
        {
            UsersServerSide.ClientBorrowedBook(bookid,userid);
        }

        public void ClientReturnBook(string bookid, string userid)
        {
            UsersServerSide.ClientReturnBook(bookid, userid);
        }
        public List<string> GetUserBooksList(string userid)
        {
            return UsersServerSide.UserBooksList(userid);
        }

        public void SortWhichFunctionToPlay()
        {
            print.ToTheConsole("What you wish to do");
            var action = Console.ReadLine().Split(" ");
            try
            {
                var UserId = int.Parse(action[action.Length - 1]);
                if (action[0] == "+")
                {
                    RegisterNewClient(action[1], action[2], action[action.Length - 1]);
                }
                else if (action[0] == "-")
                {
                    RemoveClient(action[action.Length - 1]);
                }
                else
                {
                    foreach( var item in GetUserBooksList(action[0]))
                    {
                        print.ToTheConsole(item.ToString());
                    }
                }
            }
            catch { }
            try
            {
                string FirstBookIdChar = action[action.Length - 1].Split()[0];
                string SecoundBookIdChar = action[action.Length - 1].Split()[1];
                int TheIntPartOfTheBookId = int.Parse(action[action.Length - 1].Substring(2));
                var UserId = int.Parse(action[action.Length - 2]);
                if (action[0] == "+")
                {
                    ClientBorrowedBook(action[action.Length - 2], action[action.Length - 1]);
                }
                else if (action[0] == "-")
                {
                    ClientReturnBook(action[action.Length - 2], action[action.Length - 1]);
                }
            }
            catch { }
            try
            {
                if (action[0] == "+")
                {
                    string bookname = "";
                    for (int i = 1; i < action.Length; i++)
                    {
                        bookname += action[i] + " ";
                    }
                    AddBookToLibrary(bookname);
                }
            }
            catch { }
            
            WantToContinue();
        }

        public void WantToContinue()
        {
            int number = 0;
            print.ToTheConsole("If you want to continue working enter 1, else, enter 2.");
            do
            {
                number = int.Parse(Console.ReadLine());
            }
            while (number != 1 && number != 2);
            if (number == 1)
                SortWhichFunctionToPlay();
            else
                print.ToTheConsole("Have a nice day");
        }
    }
}