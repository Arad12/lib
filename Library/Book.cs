﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryExercise
{
    public class Book
    {
        public string ID { get; private set; }
        public string Name { get; private set; }
        public string CorrentOwnerId { get; private set; }

        public Book(string name)
        {
            this.Name = name;
            var RandomNumber = new Random();
            this.ID = $"{(char)(RandomNumber.Next(65, 90))}{(char)(RandomNumber.Next(65, 90))}{RandomNumber.Next(1000, 10000)}";
        }

        public override string ToString()
        {
            return $"Book name: {this.Name} \nBook ID: {this.ID}\n Current owner{this.CorrentOwnerId}";
        }
    }
}
