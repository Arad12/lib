﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using LibraryExercise;

namespace Library
{
    public class Program
    {
        static void Main(string[] args)
        {
            LibraryExercise.LIbrary lIbrary = new LibraryExercise.LIbrary();
            Menu menu = new Menu();
            menu.InsertToADictAllTheFunctionsOfAClass();
            menu.PrintMenu();
            menu.ChooseAnOptionFromDict();
        }
    }
}
