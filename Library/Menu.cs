﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace LibraryExercise
{
    class Menu
    {
        LIbrary ibrary = new LIbrary();
        public Dictionary<int, Tuple<string, Action>> ddictionary;
        public Menu() { ddictionary = new Dictionary<int, Tuple<string, Action>>(); }

        public void InsertToADictAllTheFunctionsOfAClass()
        {
            var methods = ibrary.GetType().GetMethods();
            for (int i = 0; i < methods.Length; i++)
            {
                ddictionary.Add(ddictionary.Count + 1, new Tuple<string, Action>(methods[i].Name, () => methods[i].Invoke(ibrary, Array.Empty<object>())));
            }
        }

        public void PrintMenu()
        {
            foreach(var item in ddictionary)
            {
                Console.WriteLine($"{item.Key} - {item.Value.Item1}");
            }
        }

        public void ChooseAnOptionFromDict()
        {
            Console.WriteLine("enter answer");
            int answer = int.Parse(Console.ReadLine());
            if (ddictionary.ContainsKey(answer))
                ddictionary[answer].Item2.Invoke();
        }
    }

}