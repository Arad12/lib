﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.Configuration;
namespace LibraryExercise
{
    public class BooksServerSide
    {
        string Bookspath = ConfigurationManager.AppSettings["BooksPath"];
        public List<string> BooksLines { get; private set; }
        public BooksServerSide()
        {
            BooksLines = File.ReadAllLines(Bookspath).OfType<string>().ToList();
        }

        public void AddBookToLibrary(string bookname)
        {
            var book = new Book(bookname);
            BooksLines.Add($"+ {book.ID} {book.Name} ");
            File.WriteAllLines(Bookspath, BooksLines);
        }


        /*
        public bool ChedckIfTheBookIsAlreadyBorrowed(string bookid)
        {
            foreach(var line in this.BooksLines)
            {
                var SplitLine = line.Split(" ");
                if (SplitLine[0] == bookid)
                {
                    if (SplitLine[2].Length == 9)
                        return true;
                }
            }
            return false;
        }

        public string HowIsTheBookOwner(string bookid)
        {
            foreach(var line in this.BooksLines)
            {
                if (line.Split(" ")[0] == bookid)
                {
                    return line.Split(" ")[2];
                }
            }
            return "0";
        } */
    }

}
