﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.IO;
using System.Configuration;

namespace LibraryExercise
{
    public class UsersServerSide
    {
        Print print = new Print();
        string Userspath = ConfigurationManager.AppSettings["UsersPath"];
        string Bookspath = ConfigurationManager.AppSettings["BooksPath"];

        public List<string> UserLines { get; private set; }
        private List<string> BooksLines;
        public UsersServerSide()
        {
            UserLines = File.ReadAllLines(Userspath).OfType<string>().ToList();
            BooksLines = File.ReadAllLines(Bookspath).OfType<string>().ToList();
        }

        public void AddClient(string firstname , string lastname, string id)
        {
            bool exist = false;
            for (int i = UserLines.Count - 1 ; i >= 0; i--)
            {
                if (UserLines.Count > 0)
                {
                    if (UserLines[i].Split(" ")[3] == id)
                    {
                        if (UserLines[i].Split(" ")[0] == "+")
                        {                           
                            print.ToTheConsole("This client is allready registered");
                            exist = true;
                            break;
                        }
                        else
                        {
                            UserLines[i] = UserLines[i].Replace("-","+");
                            File.WriteAllLines(Userspath, UserLines);
                            print.ToTheConsole("Client has registerd");
                            exist = true;
                            break;
                        }
                    }
                }
            }
            if (!exist)
            {
                UserLines.Add($"+ {firstname} {lastname} {id}");
                File.WriteAllLines(Userspath, UserLines);
            }
        }

        public void RemoveClient(string id)
        {
            for (int i = 0; i <= this.UserLines.Count - 1; i++)
            {
                var splitline = UserLines[i].Split(" ");
                if (splitline[3] == id)
                {
                    UserLines[i] = UserLines[i].Replace('+','-');
                    print.ToTheConsole("Client removed");
                    File.WriteAllLines(Userspath, UserLines);
                    break;
                }
            }
        }

        public void ClientBorrowedBook(string bookid, string userid)
        {
            bool DontHave = true;
            for (int i = 0; i <= BooksLines.Count -1; i++)
            {
                if (BooksLines[i].Split(" ")[1] == bookid)
                {
                    if (BooksLines[i].Split(" ")[BooksLines[i].Split(" ").Length - 1].Length != 9)
                    {
                        BooksLines[i] = $"{BooksLines[i]} {userid}";
                        File.WriteAllLines(Bookspath, BooksLines);
                        print.ToTheConsole("The client borrowed the book");
                        DontHave = false;
                        break;
                    }
                    else
                    {
                        DontHave = false;
                        print.ToTheConsole("This book is allready borrowed");
                        break;
                    }
                }
            }
            if (DontHave)
            {
                print.ToTheConsole("Sorry, we don't have this book");
            }

        }

        public void ClientReturnBook(string bookid, string userid)
        {
            /*    var book = from booko in BooksLines
                           where booko.Split(" ")[1] == bookid
                           select booko;
            How can I  convert it to string?
             */
            bool find = true;
            for (int i = 0; i <= BooksLines.Count - 1; i++)
            {
                if (BooksLines[i].Split(" ")[1] == bookid)
                {
                    BooksLines[i] = BooksLines[i].Substring(0, BooksLines[i].Count() - 10);
                    File.WriteAllLines(Bookspath, BooksLines);
                    find = false;
                }
            }
            if (find)
            {
                print.ToTheConsole("We could not find this book");
            }
        }

        public List<string> UserBooksList(string userid)
        {
            var books = from book in BooksLines
                        where book.Substring(book.Length - 9) == userid
                        select book;
            ///////////////////////////////////////////////////////////////

            var userbookslist = new List<string>();
            foreach(var book in BooksLines)
            {
                if(book.Substring(book.Length-9) == userid)
                {
                    userbookslist.Add(book);
                }
            }
            return userbookslist;
        }


    }
}
